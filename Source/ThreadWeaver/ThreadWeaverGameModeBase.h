// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"

#include "HAL/ThreadingBase.h"
#include "IMessageBus.h"
#include "MessageEndpoint.h"

#include "ThreadWeaver/SyncPrimitives/SimpleAtomic_Runnable.h"
#include "ThreadWeaver/SyncPrimitives/SimpleCounter_Runnable.h"
#include "ThreadWeaver/SyncPrimitives/SimpleMutex_Runnable.h"
#include "ThreadWeaver/SyncPrimitives/SimpleCollectable_Runnable.h"

#include "ThreadWeaver/Generators/NameGenerator.h"
#include "ThreadWeaver/Generators/SecondNameGenerator.h"
#include "ThreadWeaver/Generators/AgeGenerator.h"
#include "ThreadWeaver/Generators/ColorGenerator.h"

#include "ThreadWeaverGameModeBase.generated.h"

class ADumpCuteCube;

USTRUCT(BlueprintType, Atomic)
struct FNpcInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Id = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString Name = "none";
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString SecondName = "none";
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Age = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FColor Color = FColor::White;
};

USTRUCT(BlueprintType, Atomic)
struct FBusStructMessage_Name
{
	GENERATED_BODY()

	FString TextName = "none";
	FBusStructMessage_Name(FString InText = "None")
		: TextName(InText) {}
};

USTRUCT(BlueprintType, Atomic)
struct FBusStructMessage_SecondName
{
	GENERATED_BODY()

	FString TextSecondName = "none";
	FBusStructMessage_SecondName(FString InText = "None")
		: TextSecondName(InText) {}
};

USTRUCT(BlueprintType, Atomic)
struct FBusStructMessage_Age
{
	GENERATED_BODY()

	int32 Age = 0;
	FBusStructMessage_Age(int32 InAge = 0)
		: Age(InAge) {}
};

USTRUCT(BlueprintType, Atomic)
struct FBusStructMessage_Color
{
	GENERATED_BODY()

	FColor Color = FColor::White;
	FBusStructMessage_Color(FColor InColor = FColor::White)
		: Color(InColor) {}
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnUpdateByNameGeneratorThreads, FString, StringData);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnUpdateBySecondNameGeneratorThreads, FString, StringData);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnUpdateByAgeGeneratorThreads, int32, IntData);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnUpdateByColorGeneratorThreads, FColor, ColorData);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnRemoveName, FString, StringData);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnRemoveSecondName, FString, StringData);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnRemoveAge, int32, IntData);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnRemoveColor, FColor, ColorData);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnUpdateByThreadNpc, FNpcInfo, DataNpc);

/**
 *
 */
UCLASS()
class THREADWEAVER_API AThreadWeaverGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	virtual void Tick(float DeltaTime) override;
	virtual void BeginPlay() override;
	virtual void EndPlay(EEndPlayReason::Type EndPlayReason) override;

	// SimpleAtomic setting
	TArray<FRunnableThread*> CurrentRunnableGameModeThread_SimpleAtomic;
	UPROPERTY(BlueprintReadWrite, Category = "SimpleAtomic Setting")
	int32 IterationRunnableCircle = 100000;
	UPROPERTY(BlueprintReadWrite, Category = "SimpleAtomic Setting")
	int32 NumberOfThreadToCreate = 12;
	UPROPERTY(BlueprintReadWrite, Category = "SimpleAtomic Setting")
	bool bUseAtomic = true;

	// SimpleAtomic control
	UFUNCTION(BlueprintCallable)
	void CreateSimpleAtomicThread();
	UFUNCTION(BlueprintCallable)
	void GetCounterSimpleAtomic(int32& Atomic1, int32& Atomic2, int32& NonAtomic1, int32& NonAtomic2);
	UFUNCTION(BlueprintCallable)
	void ResetCounterSimpleAtomic();

	// SimpleAtomic Storage
	std::atomic_int16_t AtomicCounter1;
	std::atomic_int16_t AtomicCounter2;
	int16				NonAtomicCounter1;
	int16				NonAtomicCounter2;

	// SimpleCounter Setting
	UPROPERTY(BlueprintReadWrite)
	bool bIsUseSafeVariable = true;
	UPROPERTY(BlueprintReadWrite)
	FColor						   ColorSimpleCounter;
	class FSimpleCounter_Runnable* MyRunnableClass_SimpleCounter = nullptr;
	FRunnableThread*			   CurrentRunningGameModeThread_SimpleCounter = nullptr;

	// SimpleCounter Control
	UFUNCTION(BlueprintCallable)
	void StopSimpleCounterThread();
	UFUNCTION(BlueprintCallable)
	void KillSimpleCounterThread(bool bIsShouldWait);
	UFUNCTION(BlueprintCallable)
	void CreateSimpleCounterThread();
	UFUNCTION(BlueprintCallable)
	int32 GetSimpleCounterThread();
	UFUNCTION(BlueprintCallable)
	bool SwitchRunStateSimpleCounterThread(bool bIsPause);

	FEvent*		  SimpleCounterEvent;
	FScopedEvent* SimpleCounterScopedEvent_Ref;
	UFUNCTION(BlueprintCallable)
	void StartSimpleCounterThreadWithEvent();
	UFUNCTION(BlueprintCallable)
	void StartSimpleCounterThreadWithScopedEvent();
	UPROPERTY(BlueprintReadWrite)
	bool bIsUseFEvent = true;
	UPROPERTY(BlueprintReadWrite)
	bool bIsUseFScopedEvent = true;
	void SendRef_ScopedEvent(FScopedEvent& ScopedEvent_Ref);

	// SimpleMutex Setting
	TArray<FRunnableThread*> CurrentRunningGameModeThread_SimpleMutex;
	FRunnableThread*		 CurrentRunningGameModeThread_SimpleCollectable;

	// SimpleMutex Control
	UFUNCTION(BlueprintCallable)
	void CreateSimpleMutexThreads();
	UFUNCTION(BlueprintCallable)
	void CreateSimpleCollectableThread();
	UFUNCTION(BlueprintCallable)
	void StopSimpleMutexThreads();
	UFUNCTION(BlueprintCallable)
	TArray<FString> GetSecondNames();
	UFUNCTION(BlueprintCallable)
	TArray<FString> GetFirstNames();
	UFUNCTION(BlueprintCallable)
	TArray<int32> GetAges();
	UFUNCTION(BlueprintCallable)
	TArray<FColor> GetColors();
	UFUNCTION(BlueprintCallable)
	TArray<FNpcInfo> GetNpcInfo();

	// SimpleMutex Storage
	TArray<FString>	 FirstNames;
	FCriticalSection FirstNameMutex;

	TQueue<FString, EQueueMode::Mpsc> SecondNames;
	TArray<FString>					  CurrentSecondName;
	TQueue<int32, EQueueMode::Mpsc>	  Ages;
	TArray<int32>					  CurrentAge;
	TQueue<FColor, EQueueMode::Mpsc>  Colors;
	TArray<FColor>					  CurrentColor;

	FColor ColorForThreads;

	UPROPERTY(BlueprintAssignable)
	FOnUpdateByNameGeneratorThreads OnUpdateByNameGeneratorThreads;
	UPROPERTY(BlueprintAssignable)
	FOnUpdateBySecondNameGeneratorThreads OnUpdateBySecondNameGeneratorThreads;
	UPROPERTY(BlueprintAssignable)
	FOnUpdateByAgeGeneratorThreads OnUpdateByAgeGeneratorThreads;
	UPROPERTY(BlueprintAssignable)
	FOnUpdateByColorGeneratorThreads OnUpdateByColorGeneratorThreads;
	UPROPERTY(BlueprintAssignable)
	FOnUpdateByThreadNpc OnUpdateByThreadNpc;

	TSharedPtr<FMessageEndpoint, ESPMode::ThreadSafe> ReceiveEndpoint_NameGenerator;
	TSharedPtr<FMessageEndpoint, ESPMode::ThreadSafe> ReceiveEndpoint_SecondNameGenerator;
	TSharedPtr<FMessageEndpoint, ESPMode::ThreadSafe> ReceiveEndpoint_AgeGenerator;
	TSharedPtr<FMessageEndpoint, ESPMode::ThreadSafe> ReceiveEndpoint_ColorGenerator;
	TSharedPtr<FMessageEndpoint, ESPMode::ThreadSafe> ReceiveEndpoint_NpcInfo;

	void BusMessageHandler_NameGenerator(const FBusStructMessage_Name& Message, const TSharedRef<IMessageContext, ESPMode::ThreadSafe>& Context);
	void BusMessageHandler_SecondNameGenerator(const FBusStructMessage_SecondName& Message, const TSharedRef<IMessageContext, ESPMode::ThreadSafe>& Context);
	void BusMessageHandler_AgeGenerator(const FBusStructMessage_Age& Message, const TSharedRef<IMessageContext, ESPMode::ThreadSafe>& Context);
	void BusMessageHandler_ColorGenerator(const FBusStructMessage_Color& Message, const TSharedRef<IMessageContext, ESPMode::ThreadSafe>& Context);
	void BusMessageHandler_NpcInfo(const FNpcInfo& Message, const TSharedRef<IMessageContext, ESPMode::ThreadSafe>& Context);

	void EventMessage_NameGenerator(FString StringData);
	void EventMessage_SecondNameGenerator(FString StringData);
	void EventMessage_AgeGenerator(int32 IntData);
	void EventMessage_ColorGenerator(FColor ColorData);
	void EventMessage_NpcInfo(FNpcInfo NpcData);

	FCriticalSection NpcInfoMutex;
	TArray<FNpcInfo> NpcInfo;

	int32 CubeCount = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SimpleMutex Setting")
	TSubclassOf<class ADumpCuteCube> SpawnObjectThread;

	UPROPERTY(BlueprintAssignable)
	FOnRemoveName OnRemoveName;
	UPROPERTY(BlueprintAssignable)
	FOnRemoveSecondName OnRemoveSecondName;
	UPROPERTY(BlueprintAssignable)
	FOnRemoveAge OnRemoveAge;
	UPROPERTY(BlueprintAssignable)
	FOnRemoveColor OnRemoveColor;
};
