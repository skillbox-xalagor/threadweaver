// Fill out your copyright notice in the Description page of Project Settings.

#include "SimpleCounter_Runnable.h"
#include "ThreadWeaver/ThreadWeaverGameModeBase.h"

FSimpleCounter_Runnable::FSimpleCounter_Runnable(FColor Color, AThreadWeaverGameModeBase* OwnerActor, bool VariableMode)
{
	GameMode_Ref = OwnerActor;
	bIsUseSafeVariable = VariableMode;
}

FSimpleCounter_Runnable::~FSimpleCounter_Runnable()
{
}

bool FSimpleCounter_Runnable::Init()
{
	return true;
}

uint32 FSimpleCounter_Runnable::Run()
{
	// FScopedEvent
	if (GameMode_Ref->bIsUseFScopedEvent)
	{
		{
			FScopedEvent SimpleCounterScopedEvent;
			GameMode_Ref->SendRef_ScopedEvent(SimpleCounterScopedEvent);
		}
	}

	// FEvent
	if (GameMode_Ref->SimpleCounterEvent)
	{
		GameMode_Ref->SimpleCounterEvent->Wait(10000);
		if (GameMode_Ref->SimpleCounterEvent)
		{
			GameMode_Ref->SimpleCounterEvent->Wait(10000);
		}
	}

	if (bIsUseSafeVariable)
	{
		while (!bIsStopThreadSafe)
		{
			CounterSafe.Increment();
		}
	}
	else
	{
		while (!bIsStopThread)
		{
			Counter++;
		}
	}

	return 1;
}

void FSimpleCounter_Runnable::Stop()
{
	bIsStopThread = true;
	bIsStopThreadSafe = true;
}

void FSimpleCounter_Runnable::Exit()
{
	GameMode_Ref = nullptr;
}
