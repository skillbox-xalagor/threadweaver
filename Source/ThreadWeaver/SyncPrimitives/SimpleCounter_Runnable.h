// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

class AThreadWeaverGameModeBase;

/**
 *
 */
class THREADWEAVER_API FSimpleCounter_Runnable : public FRunnable
{
public:
	FSimpleCounter_Runnable(FColor Color, AThreadWeaverGameModeBase* OwnerActor, bool VariableMode);
	virtual ~FSimpleCounter_Runnable() override;

	// Safe Variable
	FThreadSafeBool	   bIsStopThreadSafe = FThreadSafeBool(false);
	FThreadSafeCounter CounterSafe = FThreadSafeCounter(0);

	// Not Safe Variable
	bool  bIsStopThread = false;
	int32 Counter = 0;

	// Setting
	AThreadWeaverGameModeBase* GameMode_Ref;
	bool					   bIsUseSafeVariable;

	virtual bool   Init() override;
	virtual uint32 Run() override;
	virtual void   Stop() override;
	virtual void   Exit() override;
};
