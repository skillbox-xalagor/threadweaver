#pragma once

#include "CoreMinimal.h"

class AThreadWeaverGameModeBase;

class THREADWEAVER_API FSimpleAtomic_Runnable : public FRunnable
{
public:
	FSimpleAtomic_Runnable(FColor Color, AThreadWeaverGameModeBase* OwnerActor, uint32 NeedIteration, bool SeparateLogic, bool bIsUseAtomic);
	virtual ~FSimpleAtomic_Runnable() override;

	virtual bool   Init() override;
	virtual uint32 Run() override;
	virtual void   Stop() override;
	virtual void   Exit() override;

	int						   NumberIteration = 0;
	AThreadWeaverGameModeBase* GameMode_Ref = nullptr;
	bool					   bIsStopThread = false;
	bool					   bUseAtomicFlag = true;
	bool					   SeparateLogicFlag = false;
};
