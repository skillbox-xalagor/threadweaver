// Fill out your copyright notice in the Description page of Project Settings.

#include "SimpleCollectable_Runnable.h"
#include "MessageEndpointBuilder.h"
#include "ThreadWeaver/ThreadWeaverGameModeBase.h"

FSimpleCollectable_Runnable::FSimpleCollectable_Runnable(FColor Color, AThreadWeaverGameModeBase* OwnerActor)
{
	GameMode_Ref = OwnerActor;

	SenderEndPoint = FMessageEndpoint::Builder("Sender_FSimpleCollectable_Runnable").Build();
}

FSimpleCollectable_Runnable::~FSimpleCollectable_Runnable()
{
}

// bool FSimpleCollectable_Runnable::Init()
//{
//	return true;
// }

uint32 FSimpleCollectable_Runnable::Run()
{
	uint32 i = 0;
	while (!bIsStopCollectable)
	{
		FPlatformProcess::Sleep(2.0f);

		FNpcInfo NewNpc;
		NewNpc.Id = i;
		i++;

		int32 sizeNames = GameMode_Ref->FirstNames.Num();
		if (sizeNames > 0)
		{
			int32 randNameIndex = FMath::RandHelper(sizeNames - 1);
			GameMode_Ref->FirstNameMutex.Lock();
			NewNpc.Name = GameMode_Ref->FirstNames[randNameIndex];
			GameMode_Ref->FirstNames.RemoveAt(randNameIndex);
			GameMode_Ref->FirstNameMutex.Unlock();
			//GameMode_Ref->OnRemoveName.Broadcast(NewNpc.Name);
		}

		TArray<FString> AvailableSecondNames = GameMode_Ref->GetSecondNames();
		int32			SizeSecondNames = AvailableSecondNames.Num();
		if (SizeSecondNames > 0)
		{
			int32 randNameIndex = FMath::RandHelper(SizeSecondNames - 1);
			NewNpc.SecondName = AvailableSecondNames[randNameIndex];
			GameMode_Ref->CurrentSecondName.Remove(NewNpc.SecondName);
			//GameMode_Ref->OnRemoveSecondName.Broadcast(NewNpc.SecondName);
		}
		TArray<int32> AvailableAges = GameMode_Ref->GetAges();
		int32			SizeAges = AvailableAges.Num();
		if (SizeAges > 0)
		{
			int32 randNameIndex = FMath::RandHelper(SizeAges - 1);
			NewNpc.Age = AvailableAges[randNameIndex];
			GameMode_Ref->CurrentAge.Remove(NewNpc.Age);
			//GameMode_Ref->OnRemoveAge.Broadcast(NewNpc.Age);
		}
		TArray<FColor> AvailableColors = GameMode_Ref->GetColors();
		int32			SizeColors = AvailableColors.Num();
		if (SizeColors > 0)
		{
			int32 randNameIndex = FMath::RandHelper(SizeColors - 1);
			NewNpc.Color = AvailableColors[randNameIndex];
			GameMode_Ref->CurrentColor.Remove(NewNpc.Color);
			//GameMode_Ref->OnRemoveColor.Broadcast(NewNpc.Color);
		}
		{
			FScopeLock NpsScopedLock(&GameMode_Ref->NpcInfoMutex);
			GameMode_Ref->NpcInfo.Add(NewNpc);
		}
		if (SenderEndPoint.IsValid())
		{
			SenderEndPoint->Publish<FNpcInfo>(new FNpcInfo(NewNpc));
		}
	}
	return 1;
}

void FSimpleCollectable_Runnable::Stop()
{
	bIsStopCollectable = true;
}

void FSimpleCollectable_Runnable::Exit()
{
	if (SenderEndPoint.IsValid())
	{
		SenderEndPoint.Reset();
	}
}
