#pragma once

#include "CoreMinimal.h"
#include "MessageEndpoint.h"

class AThreadWeaverGameModeBase;

class THREADWEAVER_API FSimpleMutex_Runnable : public FRunnable
{
public:
	FSimpleMutex_Runnable(FColor Color, AThreadWeaverGameModeBase* OwnerActor);
	//virtual ~FSimpleMutex_Runnable() override;

	virtual bool   Init() override;
	//virtual uint32 Run() override;
	virtual void   Stop() override;
	virtual void   Exit() override;

	FColor					   ThreadColor;
	AThreadWeaverGameModeBase* GameMode_Ref = nullptr;
	bool					   bIsGenerateSecondName;
	FThreadSafeBool			   bIsStopGenerator = false;

	int8 GetRandom(int8 min, int8 max);
	bool GetRandom();

	TSharedPtr<FMessageEndpoint, ESPMode::ThreadSafe> SenderEndpoint;
};