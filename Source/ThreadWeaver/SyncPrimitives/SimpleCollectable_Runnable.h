#pragma once

#include "CoreMinimal.h"
#include "MessageEndpoint.h"

class AThreadWeaverGameModeBase;

class THREADWEAVER_API FSimpleCollectable_Runnable : public FRunnable
{
public:
	FSimpleCollectable_Runnable(FColor Color, AThreadWeaverGameModeBase* OwnerActor);
	virtual ~FSimpleCollectable_Runnable() override;

	// virtual bool Init() override;
	virtual uint32 Run() override;
	virtual void   Stop() override;
	virtual void   Exit() override;

	AThreadWeaverGameModeBase* GameMode_Ref = nullptr;

	FThreadSafeBool									  bIsStopCollectable = false;
	TSharedPtr<FMessageEndpoint, ESPMode::ThreadSafe> SenderEndPoint;
};