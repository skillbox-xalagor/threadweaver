#include "SimpleAtomic_Runnable.h"
#include "ThreadWeaver/ThreadWeaverGameModeBase.h"

FSimpleAtomic_Runnable::FSimpleAtomic_Runnable(FColor Color, AThreadWeaverGameModeBase* OwnerActor, uint32 NeedIteration, bool SeparateLogic, bool bIsUseAtomic)
{
	GameMode_Ref = OwnerActor;
	NumberIteration = NeedIteration;
	bUseAtomicFlag = bIsUseAtomic;
	SeparateLogicFlag = SeparateLogic;
	UE_LOG(LogTemp, Error, TEXT("FSimpleAtomic_Runnable::FSimpleAtomic_Runnable"));
}

FSimpleAtomic_Runnable::~FSimpleAtomic_Runnable()
{
	UE_LOG(LogTemp, Error, TEXT("FSimpleAtomic_Runnable::~FSimpleAtomic_Runnable"));
}

bool FSimpleAtomic_Runnable::Init()
{
	UE_LOG(LogTemp, Error, TEXT("FSimpleAtomic_Runnable::Init"));
	return true;
}

uint32 FSimpleAtomic_Runnable::Run()
{
	// FPlatformProcess::Sleep(1.0f); //use sleep for debug
	for (int i = 0; i < NumberIteration; ++i)
	{
		if (SeparateLogicFlag)
		{
			if (bUseAtomicFlag)
			{
				GameMode_Ref->AtomicCounter1.fetch_add(1);
				FPlatformAtomics::InterlockedIncrement(&GameMode_Ref->NonAtomicCounter1);
			}
			else
				GameMode_Ref->NonAtomicCounter1++;
		}
		else
		{
			if (bUseAtomicFlag)
			{
				GameMode_Ref->AtomicCounter2.fetch_add(1);
				FPlatformAtomics::InterlockedIncrement(&GameMode_Ref->NonAtomicCounter2);
			}
			else
				GameMode_Ref->NonAtomicCounter2++;
		}
	}
	return 0;
}

void FSimpleAtomic_Runnable::Stop()
{
	UE_LOG(LogTemp, Error, TEXT("FSimpleAtomic_Runnable::Stop"));
}

void FSimpleAtomic_Runnable::Exit()
{
	GameMode_Ref = nullptr;
	UE_LOG(LogTemp, Error, TEXT("FSimpleAtomic_Runnable::Exit"));
}
