// Fill out your copyright notice in the Description page of Project Settings.

#include "ThreadWeaverGameModeBase.h"
#include "MessageEndpointBuilder.h"
#include "Misc/ScopeLock.h"
#include "DumpCuteCube.h"

void AThreadWeaverGameModeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	/*uint32 ThreadID = FPlatformTLS::GetCurrentThreadId();

	UE_LOG(LogTemp, Error, TEXT("AThreadWeaverGameModeBase::Tick ID - %d, Name - %s"), ThreadID, *FThreadManager::GetThreadName(ThreadID));

	FThreadManager::Get().ForEachThread([&](uint32 ThreadId, FRunnableThread* Runnable) {
		FString myThreadType = "none";
		switch (Runnable->GetThreadPriority())
		{
			case TPri_Normal:
				myThreadType = "TPri_Normal";
				break;
			case TPri_AboveNormal:
				myThreadType = "TPri_AboveNormal";
				break;
			case TPri_BelowNormal:
				myThreadType = "TPri_BelowNormal";
				break;
			case TPri_Highest:
				myThreadType = "TPri_Highest";
				break;
			case TPri_Lowest:
				myThreadType = "TPri_Lowest";
				break;
			case TPri_SlightlyBelowNormal:
				myThreadType = "TPri_SlightlyBelowNormal";
				break;
			case TPri_TimeCritical:
				myThreadType = "TPri_TimeCritical";
				break;
			case TPri_Num:
				myThreadType = "TPri_Num";
				break;
			default:
				break;
		}
		UE_LOG(LogTemp, Error, TEXT("AThreadWeaverGameModeBase::Tick Id - %d Name - %s, Priority - %s"), ThreadId, *Runnable->GetThreadName(), *myThreadType);
	});*/
}

void AThreadWeaverGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	ReceiveEndpoint_NameGenerator = FMessageEndpoint::Builder("Receiver_AThreadWeaverGameModeBase").Handling<FBusStructMessage_Name>(this, &AThreadWeaverGameModeBase::BusMessageHandler_NameGenerator);

	if (ReceiveEndpoint_NameGenerator.IsValid())
	{
		ReceiveEndpoint_NameGenerator->Subscribe<FBusStructMessage_Name>();
	}

	ReceiveEndpoint_SecondNameGenerator = FMessageEndpoint::Builder("Receiver_AThreadWeaverGameModeBase").Handling<FBusStructMessage_SecondName>(this, &AThreadWeaverGameModeBase::BusMessageHandler_SecondNameGenerator);

	if (ReceiveEndpoint_SecondNameGenerator.IsValid())
	{
		ReceiveEndpoint_SecondNameGenerator->Subscribe<FBusStructMessage_SecondName>();
	}

	ReceiveEndpoint_AgeGenerator = FMessageEndpoint::Builder("Receiver_AThreadWeaverGameModeBase").Handling<FBusStructMessage_Age>(this, &AThreadWeaverGameModeBase::BusMessageHandler_AgeGenerator);

	if (ReceiveEndpoint_AgeGenerator.IsValid())
	{
		ReceiveEndpoint_AgeGenerator->Subscribe<FBusStructMessage_Age>();
	}

	ReceiveEndpoint_ColorGenerator = FMessageEndpoint::Builder("Receiver_AThreadWeaverGameModeBase").Handling<FBusStructMessage_Color>(this, &AThreadWeaverGameModeBase::BusMessageHandler_ColorGenerator);

	if (ReceiveEndpoint_ColorGenerator.IsValid())
	{
		ReceiveEndpoint_ColorGenerator->Subscribe<FBusStructMessage_Color>();
	}

	ReceiveEndpoint_NpcInfo = FMessageEndpoint::Builder("Receiver_AThreadExampleGameModeBase").Handling<FNpcInfo>(this, &AThreadWeaverGameModeBase::BusMessageHandler_NpcInfo);

	if (ReceiveEndpoint_NpcInfo.IsValid())
	{
		ReceiveEndpoint_NpcInfo->Subscribe<FNpcInfo>();
	}
}

void AThreadWeaverGameModeBase::EndPlay(EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	StopSimpleCounterThread();
	StopSimpleMutexThreads();

	if (ReceiveEndpoint_NameGenerator.IsValid())
	{
		ReceiveEndpoint_NameGenerator.Reset();
	}
	if (ReceiveEndpoint_NpcInfo.IsValid())
	{
		ReceiveEndpoint_NpcInfo.Reset();
	}
}

void AThreadWeaverGameModeBase::CreateSimpleAtomicThread()
{
	for (int i = 0; i < NumberOfThreadToCreate; i++)
	{
		FColor						  color;
		class FSimpleAtomic_Runnable* MyRunnableClass_SimpleAtomic = new FSimpleAtomic_Runnable(color, this, IterationRunnableCircle, i % 2 == 0, bUseAtomic);

		CurrentRunnableGameModeThread_SimpleAtomic.Add(FRunnableThread::Create(MyRunnableClass_SimpleAtomic, TEXT("SimpleAtomic thread"), 0, EThreadPriority::TPri_Lowest));
	}
}

void AThreadWeaverGameModeBase::GetCounterSimpleAtomic(int32& Atomic1, int32& Atomic2, int32& NonAtomic1, int32& NonAtomic2)
{
	if (bUseAtomic)
	{
		NonAtomic1 = FPlatformAtomics::AtomicRead(&NonAtomicCounter1);
		NonAtomic2 = FPlatformAtomics::AtomicRead(&NonAtomicCounter2);
		if (AtomicCounter1.is_lock_free())
		{
			Atomic1 = AtomicCounter1;
		}
		if (AtomicCounter2.is_lock_free())
		{
			Atomic2 = AtomicCounter2;
		}
	}
	else
	{
		NonAtomic1 = NonAtomicCounter1;
		NonAtomic2 = NonAtomicCounter2;
	}
}

void AThreadWeaverGameModeBase::ResetCounterSimpleAtomic()
{
	NonAtomicCounter1 = 0;
	NonAtomicCounter2 = 0;
	AtomicCounter1 = 0;
	AtomicCounter2 = 0;
}

void AThreadWeaverGameModeBase::StopSimpleCounterThread()
{
	if (!CurrentRunningGameModeThread_SimpleCounter)
	{
		if (MyRunnableClass_SimpleCounter)
		{
			CurrentRunningGameModeThread_SimpleCounter->Suspend(false);

			// Not safe
			MyRunnableClass_SimpleCounter->bIsStopThread = true;

			// Safe
			MyRunnableClass_SimpleCounter->bIsStopThreadSafe = true;

			if (SimpleCounterEvent)
			{
				SimpleCounterEvent->Trigger();
				FPlatformProcess::ReturnSynchEventToPool(SimpleCounterEvent);
				SimpleCounterEvent = nullptr;
			}

			if (SimpleCounterScopedEvent_Ref)
			{
				SimpleCounterScopedEvent_Ref->Trigger();
				SimpleCounterScopedEvent_Ref = nullptr;
			}

			CurrentRunningGameModeThread_SimpleCounter->WaitForCompletion();
			CurrentRunningGameModeThread_SimpleCounter = nullptr;
			MyRunnableClass_SimpleCounter = nullptr;
		}
	}
}

void AThreadWeaverGameModeBase::KillSimpleCounterThread(bool bIsShouldWait)
{
	if (CurrentRunningGameModeThread_SimpleCounter)
	{
		CurrentRunningGameModeThread_SimpleCounter->Suspend(false);
		CurrentRunningGameModeThread_SimpleCounter->Kill(bIsShouldWait);
		CurrentRunningGameModeThread_SimpleCounter = nullptr;
		MyRunnableClass_SimpleCounter = nullptr;
	}
}

void AThreadWeaverGameModeBase::CreateSimpleCounterThread()
{
	if (bIsUseFEvent)
	{
		SimpleCounterEvent = FPlatformProcess::GetSynchEventFromPool();
	}
	if (!CurrentRunningGameModeThread_SimpleCounter)
	{
		if (!MyRunnableClass_SimpleCounter)
		{
			MyRunnableClass_SimpleCounter = new FSimpleCounter_Runnable(ColorSimpleCounter, this, bIsUseSafeVariable);
		}
		CurrentRunningGameModeThread_SimpleCounter = FRunnableThread::Create(MyRunnableClass_SimpleCounter, TEXT("SimpleCounter Thread"), 0, EThreadPriority::TPri_Normal);
	}
}

int32 AThreadWeaverGameModeBase::GetSimpleCounterThread()
{
	int32 result = -1;
	if (MyRunnableClass_SimpleCounter)
	{
		if (MyRunnableClass_SimpleCounter->bIsUseSafeVariable)
		{
			result = MyRunnableClass_SimpleCounter->CounterSafe.GetValue();
		}
		else
		{
			result = MyRunnableClass_SimpleCounter->Counter;
		}
	}
	return result;
}

bool AThreadWeaverGameModeBase::SwitchRunStateSimpleCounterThread(bool bIsPause)
{
	if (CurrentRunningGameModeThread_SimpleCounter)
	{
		CurrentRunningGameModeThread_SimpleCounter->Suspend(bIsPause);
	}
	return !bIsPause;
}

void AThreadWeaverGameModeBase::StartSimpleCounterThreadWithEvent()
{
	if (SimpleCounterEvent)
	{
		SimpleCounterEvent->Trigger();
	}
}

void AThreadWeaverGameModeBase::StartSimpleCounterThreadWithScopedEvent()
{
	if (SimpleCounterScopedEvent_Ref)
	{
		SimpleCounterScopedEvent_Ref->Trigger();
		SimpleCounterScopedEvent_Ref = nullptr;
	}
}

void AThreadWeaverGameModeBase::SendRef_ScopedEvent(FScopedEvent& ScopedEvent_Ref)
{
	SimpleCounterScopedEvent_Ref = &ScopedEvent_Ref;
}

void AThreadWeaverGameModeBase::CreateSimpleMutexThreads()
{
	for (int i = 0; i < 2; i++)
	{
		CurrentRunningGameModeThread_SimpleMutex.Add(FRunnableThread::Create(new FNameGenerator(ColorForThreads, this), TEXT("NameGenerator Thread"), 0, EThreadPriority::TPri_Lowest));
		CurrentRunningGameModeThread_SimpleMutex.Add(FRunnableThread::Create(new FSecondNameGenerator(ColorForThreads, this), TEXT("SecondNameGenerator Thread"), 0, EThreadPriority::TPri_Lowest));
		CurrentRunningGameModeThread_SimpleMutex.Add(FRunnableThread::Create(new FAgeGenerator(ColorForThreads, this), TEXT("AgeGenerator Thread"), 0, EThreadPriority::TPri_Lowest));
		CurrentRunningGameModeThread_SimpleMutex.Add(FRunnableThread::Create(new FColorGenerator(ColorForThreads, this), TEXT("ColorGenerator Thread"), 0, EThreadPriority::TPri_Lowest));
	}
}

void AThreadWeaverGameModeBase::CreateSimpleCollectableThread()
{
	class FSimpleCollectable_Runnable* MyRunnableClass_SimpleCollectable = new FSimpleCollectable_Runnable(ColorForThreads, this);
	CurrentRunningGameModeThread_SimpleMutex.Add(FRunnableThread::Create(MyRunnableClass_SimpleCollectable, TEXT("SimpleCollectable Thread"), 0, EThreadPriority::TPri_Lowest));
}

void AThreadWeaverGameModeBase::StopSimpleMutexThreads()
{
	if (CurrentRunningGameModeThread_SimpleMutex.Num() > 0)
	{
		for (auto RunnableThread : CurrentRunningGameModeThread_SimpleMutex)
		{
			if (RunnableThread)
			{
				RunnableThread->Kill(true);
			}
		}
		CurrentRunningGameModeThread_SimpleMutex.Empty();
	}
	if (CurrentRunningGameModeThread_SimpleCollectable)
	{
		CurrentRunningGameModeThread_SimpleCollectable->Kill(true);
		CurrentRunningGameModeThread_SimpleCollectable = nullptr;
	}
}

TArray<FString> AThreadWeaverGameModeBase::GetSecondNames()
{
	TArray<FString> result;
	FString			OneRead;
	while (SecondNames.Dequeue(OneRead))
	{
		result.Add(OneRead);
	}

	CurrentSecondName.Append(result);

	return CurrentSecondName;
}

TArray<int32> AThreadWeaverGameModeBase::GetAges()
{
	TArray<int32> result;
	int32		  OneRead;
	while (Ages.Dequeue(OneRead))
	{
		result.Add(OneRead);
	}

	CurrentAge.Append(result);

	return CurrentAge;
}

TArray<FColor> AThreadWeaverGameModeBase::GetColors()
{
	TArray<FColor> result;
	FColor			OneRead;
	while (Colors.Dequeue(OneRead))
	{
		result.Add(OneRead);
	}

	CurrentColor.Append(result);

	return CurrentColor;
}

TArray<FString> AThreadWeaverGameModeBase::GetFirstNames()
{
	{
		FScopeLock myScopedLock(&FirstNameMutex);
		return FirstNames;
	}
}

TArray<FNpcInfo> AThreadWeaverGameModeBase::GetNpcInfo()
{
	TArray<FNpcInfo> result;
	return result;
}

void AThreadWeaverGameModeBase::BusMessageHandler_NameGenerator(const FBusStructMessage_Name& Message, const TSharedRef<IMessageContext, ESPMode::ThreadSafe>& Context)
{
	EventMessage_NameGenerator(Message.TextName);
}

void AThreadWeaverGameModeBase::BusMessageHandler_SecondNameGenerator(const FBusStructMessage_SecondName& Message, const TSharedRef<IMessageContext, ESPMode::ThreadSafe>& Context)
{
	EventMessage_SecondNameGenerator(Message.TextSecondName);
}

void AThreadWeaverGameModeBase::BusMessageHandler_AgeGenerator(const FBusStructMessage_Age& Message, const TSharedRef<IMessageContext, ESPMode::ThreadSafe>& Context)
{
	EventMessage_AgeGenerator(Message.Age);
}

void AThreadWeaverGameModeBase::BusMessageHandler_ColorGenerator(const FBusStructMessage_Color& Message, const TSharedRef<IMessageContext, ESPMode::ThreadSafe>& Context)
{
	EventMessage_ColorGenerator(Message.Color);
}

void AThreadWeaverGameModeBase::BusMessageHandler_NpcInfo(const FNpcInfo& Message, const TSharedRef<IMessageContext, ESPMode::ThreadSafe>& Context)
{
	EventMessage_NpcInfo(Message);
}

void AThreadWeaverGameModeBase::EventMessage_NameGenerator(FString StringData)
{
	OnUpdateByNameGeneratorThreads.Broadcast(StringData);
}

void AThreadWeaverGameModeBase::EventMessage_SecondNameGenerator(FString StringData)
{
	OnUpdateBySecondNameGeneratorThreads.Broadcast(StringData);
}

void AThreadWeaverGameModeBase::EventMessage_AgeGenerator(int32 IntData)
{
	OnUpdateByAgeGeneratorThreads.Broadcast(IntData);
}

void AThreadWeaverGameModeBase::EventMessage_ColorGenerator(FColor ColorData)
{
	OnUpdateByColorGeneratorThreads.Broadcast(ColorData);
}

void AThreadWeaverGameModeBase::EventMessage_NpcInfo(FNpcInfo NpcData)
{
	OnUpdateByThreadNpc.Broadcast(NpcData);

	UWorld* myWorld = GetWorld();
	if (myWorld && SpawnObjectThread)
	{
		FVector		   SpawnLoc = FVector(1200.0f, 100.0f * CubeCount, 500.0f);
		FRotator	   SpawnRot;
		ADumpCuteCube* myCuteCube;
		myCuteCube = Cast<ADumpCuteCube>(myWorld->SpawnActor(SpawnObjectThread, &SpawnLoc, &SpawnRot, FActorSpawnParameters()));
		if (myCuteCube)
		{
			myCuteCube->Init(NpcData);
			CubeCount++;
		}
	}
}
