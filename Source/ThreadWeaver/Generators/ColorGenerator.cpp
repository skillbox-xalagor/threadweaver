#include "ColorGenerator.h"
#include "MessageEndpointBuilder.h"
#include "ThreadWeaver/ThreadWeaverGameModeBase.h"

FColorGenerator::FColorGenerator(FColor Color, AThreadWeaverGameModeBase* OwnerActor)
	: FSimpleMutex_Runnable::FSimpleMutex_Runnable(Color, OwnerActor)
{
	// IMessageBus sender init
	SenderEndpoint = FMessageEndpoint::Builder("Sender_FColorGenerator").Build();
}

uint32 FColorGenerator::Run()
{
	TArray<FColor> ColorVariants = {
		FColor::White,
		FColor::Black,
		FColor::Transparent,
		FColor::Red,
		FColor::Green,
		FColor::Blue,
		FColor::Yellow,
		FColor::Cyan,
		FColor::Magenta,
		FColor::Orange,
		FColor::Purple,
		FColor::Turquoise,
		FColor::Silver,
		FColor::Emerald,
	};
	while (!bIsStopGenerator)
	{
		FPlatformProcess::Sleep(1.0f);
		FColor Result = ColorVariants[GetRandom(0, ColorVariants.Num() - 1)];

		GameMode_Ref->Colors.Enqueue(Result);

		if (SenderEndpoint.IsValid())
			SenderEndpoint->Publish<FBusStructMessage_Color>(new FBusStructMessage_Color(Result));
	}

	return 1;
}