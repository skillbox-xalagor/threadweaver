#pragma once

#include "CoreMinimal.h"
#include "ThreadWeaver/SyncPrimitives/SimpleMutex_Runnable.h"

class AThreadWeaverGameModeBase;

class THREADWEAVER_API FAgeGenerator : public FSimpleMutex_Runnable
{
public:
	FAgeGenerator(FColor Color, AThreadWeaverGameModeBase* OwnerActor);
	virtual uint32 Run() override;
};
