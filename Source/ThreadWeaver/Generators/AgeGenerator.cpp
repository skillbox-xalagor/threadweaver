#include "AgeGenerator.h"
#include "MessageEndpointBuilder.h"
#include "ThreadWeaver/ThreadWeaverGameModeBase.h"

FAgeGenerator::FAgeGenerator(FColor Color, AThreadWeaverGameModeBase* OwnerActor)
	: FSimpleMutex_Runnable::FSimpleMutex_Runnable(Color, OwnerActor)
{
	// IMessageBus sender init
	SenderEndpoint = FMessageEndpoint::Builder("Sender_FAgeGenerator").Build();
}

uint32 FAgeGenerator::Run()
{
	while (!bIsStopGenerator)
	{
		FPlatformProcess::Sleep(1.0f);
		int32 Result = GetRandom(1, 50);

		GameMode_Ref->Ages.Enqueue(Result);

		if (SenderEndpoint.IsValid())
			SenderEndpoint->Publish<FBusStructMessage_Age>(new FBusStructMessage_Age(Result));
	}

	return 1;
}
