#include "NameGenerator.h"
#include "MessageEndpointBuilder.h"
#include "ThreadWeaver/ThreadWeaverGameModeBase.h"

FNameGenerator::FNameGenerator(FColor Color, AThreadWeaverGameModeBase* OwnerActor)
	: FSimpleMutex_Runnable::FSimpleMutex_Runnable(Color, OwnerActor)
{
	// IMessageBus sender init
	SenderEndpoint = FMessageEndpoint::Builder("Sender_FNameGenerator").Build();
}

uint32 FNameGenerator::Run()
{
	TArray<FString> VowelLetter{ "a", "e", "i", "o", "u", "y" };
	TArray<FString> ConsonantLetter{ "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "w", "x", "z" };

	while (!bIsStopGenerator)
	{
		FString Result;
		bool	bIsStartWithVowel = GetRandom();
		int8	RandomSizeName;
		RandomSizeName = GetRandom(3, 5);

		for (int i = 0; i < RandomSizeName; i++)
		{
			if (bIsStartWithVowel)
			{
				if (i % 2)
				{
					Result.Append(VowelLetter[GetRandom(0, VowelLetter.Num() - 1)]);
				}
				else
				{
					Result.Append(ConsonantLetter[GetRandom(0, ConsonantLetter.Num() - 1)]);
				}
			}
			else
			{
				if (i % 2)
				{
					Result.Append(ConsonantLetter[GetRandom(0, ConsonantLetter.Num() - 1)]);
				}
				else
				{
					Result.Append(VowelLetter[GetRandom(0, VowelLetter.Num() - 1)]);
				}
			}
		}

		FPlatformProcess::Sleep(1.0f);
		GameMode_Ref->FirstNameMutex.Lock();
		GameMode_Ref->FirstNames.Add(Result);
		GameMode_Ref->FirstNameMutex.Unlock();

		if (SenderEndpoint.IsValid())
			SenderEndpoint->Publish<FBusStructMessage_Name>(new FBusStructMessage_Name(Result));
	}

	return 1;
}
